//
//  ViewController.swift
//  SampleAppiBeacon_POC
//
//  Created by Salman Qureshi on 7/16/20.
//  Copyright © 2020 Salman Qureshi. All rights reserved.
//

import UIKit
import CoreLocation

class ViewController: UIViewController {

    let locationManager = CLLocationManager()
    fileprivate var beacons: [CLBeacon] = []
    
    var txService = [UUID]()
    
    var beaconRegion: CLBeaconRegion?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
    }
    
    func requestLocationUpdates() {
        locationManager.requestAlwaysAuthorization()
        locationManager.delegate = self
        locationManager.allowsBackgroundLocationUpdates = true
        locationManager.pausesLocationUpdatesAutomatically = true
        locationManager.distanceFilter = kCLDistanceFilterNone
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        Constant.allUsersToScanFor.forEach { (userID) in
            if let eachUUID = UUID(uuidString: userID) {
                txService.append(eachUUID)
            }
        }
        requestLocationUpdates()
        startRangingBeacon()
    }
    
    func startRangingBeacon() {
        
        
        txService.enumerated().forEach { (arg0) in
            let (_, _) = arg0
            if #available(iOS 13, *) {
                beaconRegion = CLBeaconRegion(uuid: arg0.element, identifier: "\(arg0.element)")
            } else {
                beaconRegion = CLBeaconRegion(proximityUUID: arg0.element, identifier: "\(arg0.element)")
            }
            
            locationManager.startRangingBeacons(in: beaconRegion!)
        }
    }
    
    private func stopRanging() {
        
        locationManager.rangedRegions.forEach { (region) in
            self.locationManager.stopRangingBeacons(in: region as! CLBeaconRegion)
        }
        
        txService.removeAll()
    }
    
    func nameForProximity(_ proximity: CLProximity) -> String {
        switch proximity {
        case .unknown:
            return "Unknown"
        case .immediate:
            return "Immediate"
        case .near:
            return "Near"
        case .far:
            return "Far"
        @unknown default:
            fatalError()
        }
    }
}

extension ViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didRangeBeacons beacons: [CLBeacon], in region: CLBeaconRegion) {
        
        let knownBeacons = beacons.filter{ $0.proximity != CLProximity.unknown }
        
        if knownBeacons.count > 0 {
            let nearestBeacon = knownBeacons.first!
            let major = CLBeaconMajorValue(truncating: nearestBeacon.major)
            let minor = CLBeaconMinorValue(truncating: nearestBeacon.minor)
            
            let accuracy = String(format: "%.2f", nearestBeacon.accuracy)
            
            print("Proximity: \(nameForProximity(nearestBeacon.proximity)) Accuracy: approx. \(accuracy)m, RSSI: \(nearestBeacon.rssi), UUID: \(nearestBeacon.uuid), major: \(major), minor: \(minor)")
        }
    }
    
    func locationManager(_ manager: CLLocationManager, monitoringDidFailFor region: CLRegion?, withError error: Error) {
      print("Failed monitoring region: \(error.localizedDescription)")
    }
      
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
      print("Location manager failed: \(error.localizedDescription)")
    }
}

