//
//  Constant.swift
//  SampleAppBLE_POC
//
//  Created by Salman Qureshi on 7/15/20.
//  Copyright © 2020 Salman Qureshi. All rights reserved.
//

import Foundation


struct Constant {
    
    static var allUsersToScanFor = ["032d1250-bb98-11ea-9f50-5f43f06c6063", "055f4ed0-bc4c-11ea-bcc4-f798f0abb903","2cccd1e0-b84b-11ea-be80-d95aae86e0c9","284cf160-aa9d-11ea-85fc-a1166ba957a1","3bf1b4b0-acac-11ea-820f-7d10f29ce55b","6e980330-b7ae-11ea-b25e-a18877889d56","0ae27c40-bdca-11ea-8c1c-6798bf24f561","1bac3c10-bdc9-11ea-8c1c-6798bf24f561","32a847c0-bec7-11ea-b1ff-39bd201d8986","379c6020-bdca-11ea-8c1c-6798bf24f561","42fc3a00-acac-11ea-b0a3-a93fc9915464","4b01e830-acac-11ea-820f-7d10f29ce55b","5cfa5870-bdcc-11ea-83bd-f5633d29d886","47384580-b21c-11ea-8cc4-bd1d0e316729","5d6918e0-b120-11ea-b3f0-cbdb4830409f","648792b0-bec6-11ea-9f55-05a3a9627b5c","734bffc0-bdcc-11ea-8c1c-6798bf24f561","73b2b670-b10b-11ea-8b3c-4b402e49dac4","7641d840-af07-11ea-845f-81d81a802021","778515d0-a734-11ea-ae41-9f5390270d07","7ba3b910-a995-11ea-a37c-e1be01781420","82db5a20-a72a-11ea-ae41-9f5390270d07","84dae670-ab16-11ea-9717-ffc4bb1953d6","8994ac50-b219-11ea-964a-412bde244b57","8cdb9f20-b6e9-11ea-9e29-25ead611942b","8e627030-bdce-11ea-83bd-f5633d29d886","8ea64d30-afbb-11ea-a71f-2b380d2cfbf3","91375850-afbb-11ea-a1f7-cd9d442f16f7","937f4c80-afbb-11ea-a71f-2b380d2cfbf3","950b1ca0-afbb-11ea-a1f7-cd9d442f16f7","96b5bf60-afbb-11ea-a1f7-cd9d442f16f7","97bda370-aa1e-11ea-916f-2f0147bc86ad","98952dc0-afbb-11ea-a71f-2b380d2cfbf3","9b4afb00-bdc9-11ea-8c1c-6798bf24f561","a16fe0c0-bec5-11ea-b1ff-39bd201d8986","a3597840-bdc8-11ea-8c1c-6798bf24f561","a53d2d70-b0c4-11ea-86f6-715a063abace","a964cb20-bdca-11ea-83bd-f5633d29d886","b3cad250-aa54-11ea-ae24-d19f53771759","b46ca380-bdcf-11ea-83bd-f5633d29d886","d9ef62c0-abc6-11ea-adea-990d5217ed5e","d2190de0-a995-11ea-9e0e-6d5201ee0e18","cf1acae0-b6ef-11ea-9e29-25ead611942b","cc6fb850-b0be-11ea-ba05-c75005ff616f","ce7325a0-ac2d-11ea-820f-7d10f29ce55b","c74918c0-b0bf-11ea-b1aa-4b7cb429fc5c","cfdc06f0-afbb-11ea-a71f-2b380d2cfbf3","b8b63f30-b6ec-11ea-8b32-5175338ac8bd","dde61850-aa19-11ea-8787-9b50bf339846","ea5e56a0-bec4-11ea-9f55-05a3a9627b5c","f1b69310-abc6-11ea-adea-990d5217ed5e","f3979920-bdcf-11ea-83bd-f5633d29d886","f474edf0-bf89-11ea-8f2c-a7ed74dac807"]
    
    // Can change from allUsersToScanFor property any of those here for different phones.
    static var advertisingID = "284cf160-aa9d-11ea-85fc-a1166ba957a1" ///Change this advertising ID based on which profileID you want to advertise with. Change it for different other phones from above scanning users.
    
}
