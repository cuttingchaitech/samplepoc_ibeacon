# SamplePOC_iBeacon #
Observe the scanning results in the foreground and background with details in the console log.

### What is this repository for? ###

To test the iBeacon scanning in the background.

### How do I get set up? ###

- Deployment target is 13.0
- Before running the project, open Constant.swift file and check the property allUsersToScanFor if your profile ID is present on which you are advertising.
- Run the project on the real device and give Location permision as Allow while using from the box then move to setting -> Location -> Project -> "Always Allow"

### Who do I talk to? ###

* Repo owner or admin
Email: salman@cuttingchaitech.com